package br.com.geovane.lojasimples.service.repository;

import br.com.geovane.lojasimples.service.model.Cliente;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Faz a interface com o servidor agindo como um repositorio de clientes.
 */

public class ClienteRepository {
    private LojaService lojaService;
    private static ClienteRepository clienteRepository;
    private Cliente cliente;

    public ClienteRepository() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(LojaService.URL_HTTP_API_LOJA)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        this.lojaService = retrofit.create(LojaService.class);
    }

    /**
     * Singleton para o repositorio de Clientes.
     */
    public synchronized static ClienteRepository getInstance() {
        if (clienteRepository == null) {
            if (clienteRepository == null) {
                clienteRepository = new ClienteRepository();
            }
        }
        return clienteRepository;
    }


    public void insertCliente(Cliente cliente, Callback<Cliente> callback) {
        lojaService.postCliente(cliente).enqueue(callback);
    }

    public void updateCliente(Cliente cliente, Callback<Void> callback) {
        lojaService.putCliente( cliente.cpf,cliente).enqueue(callback);
    }

    public void getClienteByCpf(String cpf, Callback<Cliente> callback ) {
        lojaService.getClienteByCpf(cpf).enqueue(callback);
    }

    public void deleteCliente(Cliente cliente, Callback<Void> callback) {
        lojaService.deleteCliente( cliente.cpf).enqueue(callback);
    }
}
