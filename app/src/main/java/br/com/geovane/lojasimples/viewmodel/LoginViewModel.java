package br.com.geovane.lojasimples.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.databinding.ObservableField;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import br.com.geovane.lojasimples.service.model.Cliente;
import br.com.geovane.lojasimples.service.repository.LoginRepository;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * ViewModel que gerencia as atividade de login.
 */

public class LoginViewModel extends AndroidViewModel {
    private final ObservableField<String> cpfClienteAutenticado;
    private final LoginRepository loginRepo;

    private LoginViewModel(@NonNull Application application) {
        super(application);
        loginRepo = LoginRepository.getInstance();
        this.cpfClienteAutenticado = new ObservableField<>("");
    }

    /**
     * Tenta autenticar o cliente no servidor.
     * @param email email do cliente.
     * @param senha senha do cliente.
     * @param callback chamado em caso de falha ou sucesso na autenticação.
     */
    public void autenticar(String email, String senha, final LoginCallback callback) {
        if (isEmailValid(email) && isSenhaValid(senha)) {

            loginRepo.login(email, senha, new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    if (response.isSuccessful()) {
                        if (!TextUtils.isEmpty(response.body())) {
                            callback.onSuccess(response.body());
                        } else {
                            callback.onFailure();
                        }
                        cpfClienteAutenticado.set(response.body()); //autenticado com sucesso.
                    } else {//Falha de autenticação .
                        cpfClienteAutenticado.set(null);
                        callback.onFailure();
                    }
                }

                //Falha de comunicação com servidor
                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    cpfClienteAutenticado.set(null);
                    callback.onFailure();
                }
            });
        } else {
            cpfClienteAutenticado.set(null);
        }
    }

    /**
     * Vefifica se email informado é válido. Utilizado para auxiliar a view antes de tentar validar no servidor.
     *
     * @param email email a ser validado.
     * @return true se for válido e false se for inválido.
     */
    public boolean isEmailValid(String email) {
        return Cliente.isEmailValid(email);
    }

    /**
     * Vefifica se a senha informada é válida. Utilizado para auxiliar a view antes de tentar validar no servidor.
     *
     * @param senha a senha a ser validada.
     * @return true se for válido e false se for inválido.
     */
    public boolean isSenhaValid(String senha) {
        return !TextUtils.isEmpty(senha);
    }


    /**
     * Fábrica que sabe como criar um LoginViewModel.
     */
    public static class Factory extends ViewModelProvider.NewInstanceFactory {

        private Application application;

        public Factory(@NonNull Application app) {
            this.application = app;
        }

        @Override
        public <T extends ViewModel> T create(Class<T> modelClass) {
            return (T) new LoginViewModel(this.application);
        }
    }

    /**
     * Clientes devem implementar esta interface para consumir a funcionalidade de login.
     */
    public interface LoginCallback {
        void onSuccess(String cpf);

        void onFailure();
    }
}
