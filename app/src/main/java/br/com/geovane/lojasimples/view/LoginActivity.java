package br.com.geovane.lojasimples.view;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import br.com.geovane.lojasimples.R;
import br.com.geovane.lojasimples.viewmodel.EditViewModel;
import br.com.geovane.lojasimples.viewmodel.LoginViewModel;


/**
 * Tela de Login.
 */
public class LoginActivity extends AppCompatActivity {

    // UI references.
    private EditText txtEmail;
    private EditText txtSenha;
    private View progressBar;
    private View formLogin;
    private LoginViewModel loginViewModel;
    private final int MY_PERMISSIONS_REQUEST_INTERNET = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginViewModel = new LoginViewModel.Factory(getApplication()).create(LoginViewModel.class);

        // Set up the login form.
        txtEmail = findViewById(R.id.txtEmail);
        txtEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus)
                    if (!loginViewModel.isSenhaValid(txtEmail.getText().toString()))
                        txtEmail.setError(getString(R.string.error_invalid_email));
                    else txtEmail.setError(null);
            }
        });

        txtSenha = findViewById(R.id.txtSenha);
        txtSenha.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus)
                    if (!loginViewModel.isSenhaValid(txtSenha.getText().toString()))
                        txtSenha.setError(getString(R.string.error_invalid_password));
                    else txtSenha.setError(null);
            }
        });

        Button btnAutenticar = findViewById(R.id.btnAutenticar);
        btnAutenticar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                tentarAutenticar();
            }
        });

        Button btnRegistrar = findViewById(R.id.btnAutoRegistrar);
        btnRegistrar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                goToRegisterActivity();
            }
        });

        formLogin = findViewById(R.id.login_form);
        progressBar = findViewById(R.id.login_progress);
        solicitarPermissaoInternet();

    }


    private void tentarAutenticar() {
        txtEmail.setError(null);
        txtSenha.setError(null);

        String email = txtEmail.getText().toString();
        String senha = txtSenha.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Checa se usuário e senha são válidos.
        if (!loginViewModel.isSenhaValid(senha)) {
            focusView = txtSenha;
            cancel = true;
        } else if (!loginViewModel.isEmailValid(email)) {
            focusView = txtEmail;
            cancel = true;
        }

        if (cancel) {
            Snackbar.make(focusView, getString(R.string.login_failed), Snackbar.LENGTH_LONG).show();
            focusView.requestFocus();
        } else {
            //Mostra a barra de progresso e tenta login.
            showProgress(true);
            loginViewModel.autenticar(email, senha, new LoginViewModel.LoginCallback() {
                @Override
                public void onSuccess(String cpf) {
                    goToEditActivity(cpf);
                }

                @Override
                public void onFailure() {
                    showProgress(false);
                    Snackbar.make(formLogin, getString(R.string.login_failed), Snackbar.LENGTH_LONG).show();
                }
            });

        }
    }

    private void goToEditActivity(String cpf) {
        showProgress(false);
        Intent goToClienteUpdate = new Intent(getApplicationContext(), ClienteEditActivity.class);
        goToClienteUpdate.putExtra(EditViewModel.KEY_CPF_CLIENTE, cpf);
        startActivity(goToClienteUpdate);
    }

    private void goToRegisterActivity() {
        showProgress(false);
        Intent goToClienteRegister = new Intent(getApplicationContext(), ClienteEditActivity.class);
        startActivity(goToClienteRegister);
    }

    /**
     * Mostra a barra de progresso.
     */
    private void showProgress(final boolean show) {

        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        formLogin.setVisibility(show ? View.GONE : View.VISIBLE);
        formLogin.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                formLogin.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
        progressBar.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }

    private void solicitarPermissaoInternet() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.INTERNET},
                    MY_PERMISSIONS_REQUEST_INTERNET);


        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_INTERNET: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                }
                return;
            }
        }
    }
}

