package br.com.geovane.lojasimples.service.repository;

import br.com.geovane.lojasimples.service.model.CredencialLogin;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Repositório de login.
 * Comunica-se com a  camada de serviço para obter os dados de login.
 */

public class LoginRepository {

    private LojaService lojaService;
    private static LoginRepository loginRepository;

    /**
     * Constrói uma nova instancia do repostório.
     * Deve ser chamado internamente pelo método getInstance().
     */
    private LoginRepository() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(LojaService.URL_HTTP_API_LOJA)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        this.lojaService = retrofit.create(LojaService.class);
    }

    /**
     * Retorna o Singleton do repositorio de Login.
     */
    public synchronized static LoginRepository getInstance() {
        if (loginRepository == null)
            loginRepository = new LoginRepository();

        return loginRepository;
    }

    /**
     * Tenta login o cliente no servidor.
     *
     * @param email    email do cliente.
     * @param senha    senha do cliente.
     * @param callback para aguardar a resposta do servidor.
     */
    public void login(String email, String senha, Callback<String> callback) {
        lojaService.login(new CredencialLogin(email, senha)).enqueue(callback);
    }
}
