	package br.com.geovane.lojasimples.viewmodel;


    import android.app.Application;
    import android.arch.lifecycle.AndroidViewModel;
    import android.arch.lifecycle.ViewModel;
    import android.arch.lifecycle.ViewModelProvider;
    import android.databinding.ObservableField;
    import android.support.annotation.NonNull;
    import android.text.TextUtils;

    import java.util.ArrayList;

    import br.com.geovane.lojasimples.R;
    import br.com.geovane.lojasimples.service.model.Cliente;
    import br.com.geovane.lojasimples.service.repository.ClienteRepository;
    import retrofit2.Call;
    import retrofit2.Callback;
    import retrofit2.Response;

/**
 * ViewModel utilizada para gerenciar o cadastro do cliente.
 */

public class EditViewModel extends AndroidViewModel {
    public static final String KEY_CPF_CLIENTE = "CPF_CLIENTE";
    public final ObservableField<Cliente> cliente;
    private final ClienteRepository clienteRepo;


    private EditViewModel(@NonNull Application application) {
        super(application);
        cliente = new ObservableField<>();
        clienteRepo = ClienteRepository.getInstance();
    }

    /**
     * Atualliza o cliente no servidor.
     * @param callback retorno assincrono.
     */
    private void update(final ClienteOperationCallback callback) {
        clienteRepo.updateCliente(cliente.get(), new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    callback.onSuccess();
                } else {
                    callback.onFailure();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                callback.onFailure();
            }
        });
    }

    /**
     * Insere o cliente no servidor.
     * @param callback retorno assincrono.
     */
    private void insert(final ClienteOperationCallback callback) {
        clienteRepo.insertCliente(cliente.get(), new Callback<Cliente>() {
            @Override
            public void onResponse(Call<Cliente> call, Response<Cliente> response) {
                if (response.isSuccessful()) {
                    callback.onSuccess();
                } else {
                    callback.onFailure();
                }
            }

            @Override
            public void onFailure(Call<Cliente> call, Throwable t) {
                callback.onFailure();
            }
        });
    }

    /**
     * Atualizad os dados do cliente atual na view model.
     * @param cpf
     * @param email
     * @param senha
     * @param nome
     * @param telefone
     * @param endereco
     * @param municipio
     * @param estado
     * @return
     */
    public ArrayList<String> updateClienteData(String cpf, String email, String senha, String nome, String telefone, String endereco, String municipio, String estado) {
        ArrayList<String> validationErrors = new ArrayList<>();
        Cliente tempCliente = new Cliente();
        ArrayList<String> requiredFields = new ArrayList<>();
        if (Cliente.isCpfValid(cpf))
            tempCliente.cpf = cpf;
        else
            validationErrors.add(getApplication().getString(R.string.error_invalid_cpf));

        if (Cliente.isEmailValid(email))
            tempCliente.email = email;
        else
            validationErrors.add(getApplication().getString(R.string.error_invalid_email));

        if (!TextUtils.isEmpty(senha))
            tempCliente.senha = senha;
        else
            requiredFields.add("Senha");

        if (!TextUtils.isEmpty(nome))
            tempCliente.nome = nome;
        else
            requiredFields.add("Nome");
        if (!TextUtils.isEmpty(telefone))
            tempCliente.telefone = telefone;
        else
            requiredFields.add("Telefone");

        if (!TextUtils.isEmpty(endereco))
            tempCliente.endereco = endereco;
        else
            requiredFields.add("Endereço");

        if (!TextUtils.isEmpty(municipio))
            tempCliente.municipio = municipio;
        else
            requiredFields.add("Município");

        if (!TextUtils.isEmpty(estado))
            tempCliente.estado = estado;
        else
            requiredFields.add("Estado");

        if (requiredFields.size() > 0) {
            String msg = getApplication().getString(R.string.error_fields_required);
            String param = "";
            for (String field : requiredFields) {
                param = param.concat(field).concat(", ");
            }
            param = param.substring(0, param.length() - 2);
            validationErrors.add(String.format(msg.toString(), param));
        }
        this.cliente.set(tempCliente);
        return validationErrors;
    }

    /**
     * Recupera os dados do Cliente de acordo com o CPF informado.
     *
     * @param cpfCliente é o CPF do Cliente.
     * @throws InvalidCpfException é lançado quando o CPF informado possui um formato inválido.
     */
    public void getCliente(String cpfCliente, final ClienteOperationCallback callback) throws InvalidCpfException {
        if (Cliente.isCpfValid(cpfCliente)) {
            clienteRepo.getClienteByCpf(cpfCliente, new Callback<Cliente>() {
                @Override
                public void onResponse(Call<Cliente> call, Response<Cliente> response) {
                    if (response.isSuccessful()) {
                        cliente.set(response.body());
                        callback.onSuccess();
                    } else {
                        cliente.set(null);
                        callback.onFailure();
                    }
                }

                @Override
                public void onFailure(Call<Cliente> call, Throwable t) {
                    cliente.set(null);
                    callback.onFailure();
                }
            });
        } else {
            throw new InvalidCpfException(getApplication().getString(R.string.error_invalid_cpf));
        }
    }

    /**
     * Salva os dados do cliente no servidor.
     * @param callback retorno assincrono.
     */
    public void save(final ClienteOperationCallback callback) {

        //procura o cliente no servidor
        clienteRepo.getClienteByCpf(this.cliente.get().cpf, new Callback<Cliente>() {
            @Override
            public void onResponse(Call<Cliente> call, Response<Cliente> response) {
                if (response.isSuccessful()) {
                    update(callback);
                } else {
                    insert(callback);
                }
            }

            //Falha de comunicação
            @Override
            public void onFailure(Call<Cliente> call, Throwable t) {
                callback.onFailure();
            }
        });


    }

    /**
     * Apaga um cliente no servidor.
     * @param callback
     */
    public void delete(final ClienteOperationCallback callback) {
        clienteRepo.deleteCliente(cliente.get(), new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    callback.onSuccess();
                } else {
                    callback.onFailure();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                callback.onFailure();
            }
        });
    }

    /**
     * Interface para operações assincronas.
     */
    public interface ClienteOperationCallback {
        public void onSuccess();

        public void onFailure();
    }

    /**
     * Fábrica da EditViewModel.
     */
    public static class Factory extends ViewModelProvider.NewInstanceFactory {

        private Application application;

        public Factory(@NonNull Application app) {
            this.application = app;
        }

        @Override
        public <T extends ViewModel> T create(Class<T> modelClass) {
            return (T) new EditViewModel(this.application);
        }
    }

    /**
     * Utilizado para lançar exceções personalizadas para o CPF.
     */
    public class InvalidCpfException extends Exception {
        public InvalidCpfException(String message) {
            super(message);
        }
    }
}
