package br.com.geovane.lojasimples.view;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import java.util.ArrayList;

import br.com.geovane.lojasimples.R;
import br.com.geovane.lojasimples.databinding.ActivityClienteEditBinding;
import br.com.geovane.lojasimples.viewmodel.EditViewModel;

public class ClienteEditActivity extends AppCompatActivity {

    private EditViewModel editViewModel;
    private ActivityClienteEditBinding binding;
    private Button btnSalvar;
    private ImageButton btnApagar;
    private FloatingActionButton btnEditar;

    /**
     * Configura os campos na tela para edição de dados.
     * @param editing
     */
    private void setEditingMode(boolean editing) {
        if (editing) {
            binding.txtCpf.setFocusableInTouchMode(false);
            binding.txtEmail.setFocusableInTouchMode(true);
            binding.txtEndereco.setFocusableInTouchMode(true);
            binding.txtMunicipio.setFocusableInTouchMode(true);
            binding.txtNome.setFocusableInTouchMode(true);
            binding.txtSenha.setFocusableInTouchMode(true);
            binding.txtTelefone.setFocusableInTouchMode(true);

            binding.txtCpf.setFocusable(false);
            binding.txtEmail.setFocusable(true);
            binding.txtEndereco.setFocusable(true);
            binding.txtMunicipio.setFocusable(true);
            binding.txtNome.setFocusable(true);
            binding.txtSenha.setFocusable(true);
            binding.txtTelefone.setFocusable(true);

            btnApagar.setVisibility(View.GONE);
            btnSalvar.setVisibility(View.VISIBLE);
            btnEditar.hide();
        } else {
            binding.txtCpf.setFocusable(false);
            binding.txtEmail.setFocusable(false);
            binding.txtEndereco.setFocusable(false);
            binding.txtMunicipio.setFocusable(false);
            binding.txtNome.setFocusable(false);
            binding.txtSenha.setFocusable(false);
            binding.txtTelefone.setFocusable(false);
            btnApagar.setVisibility(View.VISIBLE);
            btnSalvar.setVisibility(View.GONE);
            btnEditar.show();
        }

    }

    /**
     * Tenta apagar o cliente.
     * @return caso ocorra algum erro exibe um alerta ao usuário.
     */
    private View.OnClickListener tentarApagarCliente() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                builder.setMessage(getString(R.string.confirm_cliente_delete)).setTitle(getString(R.string.confirm_client_delete_title));
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        editViewModel.delete(new EditViewModel.ClienteOperationCallback() {
                            @Override
                            public void onSuccess() {
                                Intent goToLogin = new Intent(btnApagar.getContext(), LoginActivity.class);
                                startActivity(goToLogin);
                            }

                            @Override
                            public void onFailure() {
                                Snackbar.make(btnApagar, getString(R.string.delete_cliente_error), Snackbar.LENGTH_LONG).show();
                            }
                        });
                    }
                });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                builder.show();
            }
        };
    }

    /**
     * Tenta salvar o cliente.
     * @return caso os dados sejam inválidos cancela a
     */
    private View.OnClickListener tentarSalvarCliente() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> errors = editViewModel.updateClienteData(
                        binding.txtCpf.getRawText(),
                        binding.txtEmail.getText().toString(),
                        binding.txtSenha.getText().toString(),
                        binding.txtNome.getText().toString(),
                        binding.txtTelefone.getRawText(),
                        binding.txtEndereco.getText().toString(),
                        binding.txtMunicipio.getText().toString(),
                        binding.spnEstado.getSelectedView().toString());
                if (errors.size() > 0) {
                    showErrorsDialog(errors, v.getContext());
                } else {
                    editViewModel.save(new EditViewModel.ClienteOperationCallback() {
                        @Override
                        public void onSuccess() {
                            setEditingMode(false);
                            Snackbar.make(btnSalvar, getString(R.string.update_successful), Snackbar.LENGTH_LONG).show();
                        }

                        @Override
                        public void onFailure() {
                            Snackbar.make(btnSalvar, getString(R.string.update_cliente_error), Snackbar.LENGTH_LONG).show();
                        }
                    });
                }
            }
        };
    }

    /**
     * Exibe um dialog com os erros de validação detectados.
     * @param errors lista erros.
     * @param ctx contexto da activity.
     */
    private void showErrorsDialog(ArrayList<String> errors, Context ctx) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        String msg = "";
        for (String err : errors) {
            msg = msg.concat(" - ").concat(err).concat(".\n");
        }

        builder.setMessage(msg).setTitle(getString(R.string.incosistent_data));
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.show();
    }

    /***
     * Prepra a tela para edição dos dados.
     */
    private View.OnClickListener editarCliente() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setEditingMode(true);
            }
        };
    }

    /**
     * Verifica se o cliente está se registrando ou se está editando os dados após o login.
     * Prepara a tela para edição de acordo com a opção detectada.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_cliente_edit);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        btnApagar = findViewById(R.id.btnApagar);
        btnApagar.setOnClickListener(tentarApagarCliente());
        btnSalvar = findViewById(R.id.btnSalvar);
        btnSalvar.setOnClickListener(tentarSalvarCliente());
        btnEditar = findViewById(R.id.btnEditar);
        btnEditar.setOnClickListener(editarCliente());

        editViewModel = new EditViewModel.Factory(getApplication()).create(EditViewModel.class);
        String cpf = getIntent().getStringExtra(EditViewModel.KEY_CPF_CLIENTE);

        //Cliente está logado.
        if (!TextUtils.isEmpty(cpf)) {
            try {
                editViewModel.getCliente(cpf, new EditViewModel.ClienteOperationCallback() {
                    @Override
                    public void onSuccess() {
                        setEditingMode(false);
                    }

                    @Override
                    public void onFailure() {
                        setEditingMode(true);
                    }
                });
                binding.setEditViewModel(editViewModel);

            } catch (EditViewModel.InvalidCpfException e) {
                Snackbar.make(binding.getRoot(), getString(R.string.error_invalid_cpf), Snackbar.LENGTH_LONG).show();
                finish();
            }
        }
        //Cliente está se registrando.
        else {
            setEditingMode(true);
            binding.txtCpf.setFocusableInTouchMode(true);
            binding.txtCpf.setFocusable(true);
            setTitle(getString(R.string.edit_title_register));
        }
    }
}
