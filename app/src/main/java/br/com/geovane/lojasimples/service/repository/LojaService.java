package br.com.geovane.lojasimples.service.repository;

import br.com.geovane.lojasimples.service.model.Cliente;
import br.com.geovane.lojasimples.service.model.CredencialLogin;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Interface utilizada pelo retrofit para traduzir a API em uma interface Java tipada.
 */

public interface LojaService {

    /**
     * Edereço base da API REST no servidor.
     */
    String URL_HTTP_API_LOJA = "https://lojasimplesserver.azurewebsites.net/api/";

    /**
     * Autentica um cliente no servidor de acordo com suas credenciais.
     *
     * @param credenciaisLogin contém credenciais do cliente.
     * @return cpf do cliente autenticado.
     */
    @POST("Login")
    Call<String> login(@Body CredencialLogin credenciaisLogin);

    /**
     * Insere um novo cliente no servidor.
     *
     * @param cliente objeto com os dados do cliente.
     * @return objeto com os dados inseridos.
     */
    @POST("Clientes")
    Call<Cliente> postCliente(@Body Cliente cliente);

    /**
     * Atualiza os dados do cliente no servidor.
     *
     * @param cliente objeto com os dados do cliente a ser atualizado.
     * @return objeto com os dados atualizados.
     */
    @PUT("Clientes/{cpf}")
    Call<Void> putCliente(@Path("cpf") String cpf, @Body Cliente cliente);

    /**
     * Apaga um cliente do servidor.
     *
     * @param cpf CPF do cliente a ser apagado.
     */
    @DELETE("Clientes/{cpf}")
    Call<Void> deleteCliente(@Path("cpf") String cpf);


    /**
     * Recupera os dados de um cliente através do seu cpf.
     *
     * @param cpf cpf do cliente a ser recuperado.
     * @return objeto com os dados do cliente recuperado.
     */
    @GET("Clientes/{cpf}")
    Call<Cliente> getClienteByCpf(@Path("cpf") String cpf);

}
