package br.com.geovane.lojasimples.service.model;

public class CredencialLogin {

    public CredencialLogin(String email, String senha){
        this.email = email;
        this.senha = senha;
    }

    public String email;
    public String senha;
}
