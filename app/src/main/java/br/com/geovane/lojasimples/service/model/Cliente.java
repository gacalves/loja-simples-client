package br.com.geovane.lojasimples.service.model;

import android.text.TextUtils;

import java.util.Arrays;

/**
 * Entidade que representa o Cliente.
 */

public class Cliente {
    public String cpf;
    public String email;
    public String senha;
    public String nome;
    public String endereco;
    public String municipio;
    public String estado;
    public String telefone;

    public Cliente() {
        cpf = email = senha = nome = endereco = municipio = estado = telefone = "";
    }


    /**
     * Calcula os dois últimos (um de cada vez) dígitos verificadores do CPF. Ex.: no CPF 129.587.984-63 pode ser usado para calcular o 6 ou o 3.
     *
     * @param cpf     o CPF a ser utilizado no calculo.
     * @param posicao posição do digito a ser calculado. <code>10</code> se for o primeiro ou 11 se for o segundo.
     * @return o dígito calculado.
     */
    private static int calculaDigitoVerificador(int[] cpf, int posicao) {
        int somaPeso = 0;
        for (int peso = 0; peso < posicao - 1; peso++) {
            somaPeso += cpf[peso] * (posicao - peso);
        }
        int digito = 11 - (somaPeso % 11);
        if (digito > 9)
            return 0;
        return digito;
    }

    /**
     * Divide uma String em um Array de inteiros.
     */
    private static int[] toIntArray(String text) {
        int[] intArray = new int[11];

        for (int i = 0; i <= 10; i++) {
            intArray[i] = (int) text.charAt(i) - '0';
        }
        return intArray;
    }

    /**
     * Valida se o formado do CPF está correto.
     */
    public static boolean isCpfValid(String cpf) {
        final String[] CPFS_INVALIDOS = {"00000000000", "11111111111", "22222222222",
                "33333333333", "44444444444", "55555555555", "66666666666", "77777777777",
                "88888888888", "99999999999"};
        if (TextUtils.isEmpty(cpf)) {
            return false;
        }
        cpf = cpf.replaceAll("\\D", "");
        if (!TextUtils.isDigitsOnly(cpf) || cpf.length() != 11 || Arrays.asList(CPFS_INVALIDOS).contains(cpf)) {
            return false;
        }
        //vefica se o dígito verificador é válido.
        int[] cpfArray = toIntArray(cpf);
        if (calculaDigitoVerificador(cpfArray, 10) != cpfArray[9] ||
                calculaDigitoVerificador(cpfArray, 11) != cpfArray[10]) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean isEmailValid(String email) {
        return (!TextUtils.isEmpty(email) && email.contains("@"));
    }
}
